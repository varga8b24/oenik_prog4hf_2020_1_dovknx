﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarvelMovies.Repository
{
    /// <summary>
    /// Repository műveletek általános interface-e (CRUD)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Új entitás létrehozása/beszúrása Db-be
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        decimal Create(T obj);

        /// <summary>
        /// Generikusan átadott típusnak megfelelő entitások halmazának lekérése adatbázisból
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> Read();

        /// <summary>
        /// Generikusan átadott típusnak megfelelő entitás frissítése adatbázisban
        /// </summary>
        /// <param name="obj"></param>
        bool Update(T obj);

        /// <summary>
        /// Generikusan átadott típusú elem törlése adatbázisból
        /// </summary>
        /// <param name="obj"></param>
        bool Delete(decimal id);
    }
}
