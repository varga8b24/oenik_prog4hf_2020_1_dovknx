﻿using MarvelMovies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarvelMovies.Repository
{
    /// <summary>
    /// CHARACTER táblához kapcsolódó CRUD műveleteket megvalósító osztály
    /// <see cref="IRepository{T}"/>
    /// </summary>
    public class CharacterRepository : IRepository<CHARACTER>
    {
        /// <summary>
        /// <see cref="IRepository{T}.Create(T)"/>
        /// </summary>
        /// <param name="obj">T</param>
        /// <returns>CHARID</returns>
        public decimal Create(CHARACTER obj)
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                marvelMoviesDatabaseEntities.CHARACTERS.Add(obj);
                marvelMoviesDatabaseEntities.SaveChanges();

                return obj.CHARID;
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Delete(T)"/>
        /// </summary>
        /// <param name="obj">CHARACTER object</param>
        public bool Delete(decimal id)
        {
            bool ifAcces = false;

            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                var removableCharacter = marvelMoviesDatabaseEntities.CHARACTERS.FirstOrDefault(x => x.CHARID == id);

                if (removableCharacter == null)
                {
                    return ifAcces;
                }

                ifAcces = marvelMoviesDatabaseEntities.CHARACTERS.Remove(removableCharacter) != null;
                marvelMoviesDatabaseEntities.SaveChanges();
                return ifAcces;
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Read"/>
        /// </summary>
        /// <returns>CHARACTER list</returns>
        public IEnumerable<CHARACTER> Read()
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                return marvelMoviesDatabaseEntities.CHARACTERS.ToList();
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Update(T)"/>
        /// </summary>
        /// <param name="obj">CHARACTER object</param>
        public bool Update(CHARACTER obj)
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                var characterToUpdate = marvelMoviesDatabaseEntities.CHARACTERS.FirstOrDefault(x => x.CHARID == obj.CHARID);
                
                if (characterToUpdate != null)
                {
                    characterToUpdate.CHAR_NAME = obj.CHAR_NAME;
                    characterToUpdate.NICKNAME = obj.NICKNAME;
                    characterToUpdate.ORIGIN = obj.ORIGIN;
                    characterToUpdate.MOST_SPECIAL_SKILL = obj.MOST_SPECIAL_SKILL;
                    characterToUpdate.AFTER_IW_STATUS = obj.AFTER_IW_STATUS;
                    characterToUpdate.AFTER_EG_STATUS = obj.AFTER_EG_STATUS;

                    marvelMoviesDatabaseEntities.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
