﻿using MarvelMovies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarvelMovies.Repository
{
    /// <summary>
    /// MOVIESCHARACTERSconnector táblához kapcsolódó CRUD műveleteket megvalósító osztály
    /// <see cref="IRepository{T}"/>
    /// </summary>
    public class ConnectorRepository : IRepository<MOVIESCHARACTERSconnector>
    {
        /// <summary>
        /// <see cref="IRepository{T}.Create(T)"/>
        /// </summary>
        /// <param name="obj">T</param>
        /// <returns>CONNID</returns>
        public decimal Create(MOVIESCHARACTERSconnector obj)
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                marvelMoviesDatabaseEntities.MOVIESCHARACTERSconnectors.Add(obj);
                marvelMoviesDatabaseEntities.SaveChanges();

                return obj.CONNID;
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Delete(T)"/>
        /// </summary>
        /// <param name="obj">MOVIESCHARACTERSconnector object</param>
        public bool Delete(decimal id)
        {
            bool ifAcces = false;

            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                var removableConnector = marvelMoviesDatabaseEntities.MOVIESCHARACTERSconnectors.FirstOrDefault(x => x.CONNID == id);

                if (removableConnector == null)
                {
                    return ifAcces;
                }

                ifAcces = marvelMoviesDatabaseEntities.MOVIESCHARACTERSconnectors.Remove(removableConnector) != null;
                marvelMoviesDatabaseEntities.SaveChanges();
                return ifAcces;
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Read"/>
        /// </summary>
        /// <returns>MOVIESCHARACTERSconnector list</returns>
        public IEnumerable<MOVIESCHARACTERSconnector> Read()
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                return marvelMoviesDatabaseEntities.MOVIESCHARACTERSconnectors.ToList();
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Update(T)"/>
        /// </summary>
        /// <param name="obj">MOVIESCHARACTERSconnector object</param>
        public bool Update(MOVIESCHARACTERSconnector obj)
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                var connectorToUpdate = marvelMoviesDatabaseEntities.MOVIESCHARACTERSconnectors.FirstOrDefault(x => x.CONNID == obj.CONNID);

                if (connectorToUpdate != null)
                {
                    connectorToUpdate.CHARID = obj.CHARID;
                    connectorToUpdate.MOVID = obj.MOVID;

                    marvelMoviesDatabaseEntities.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
