﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarvelMovies.Data;

namespace MarvelMovies.Repository
{
    /// <summary>
    /// COLLECTION táblához kapcsolódó CRUD műveleteket megvalósító osztály
    /// <see cref="IRepository{T}"/>
    /// </summary>
    public class CollectionRepository : IRepository<COLLECTION>
    {
        /// <summary>
        /// <see cref="IRepository{T}.Create(T)"/>
        /// </summary>
        /// <param name="obj">T</param>
        /// <returns>COLLID</returns>
        public decimal Create(COLLECTION obj)
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                marvelMoviesDatabaseEntities.COLLECTIONS.Add(obj);
                marvelMoviesDatabaseEntities.SaveChanges();

                return obj.COLLID;
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Delete(T)"/>
        /// </summary>
        /// <param name="obj">COLLECTION object</param>
        public bool Delete(decimal id)
        {
            bool ifAcces = false;

            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                var removableCollection = marvelMoviesDatabaseEntities.COLLECTIONS.FirstOrDefault(x => x.COLLID == id);

                if (removableCollection == null)
                {
                    return ifAcces;
                }

                ifAcces = marvelMoviesDatabaseEntities.COLLECTIONS.Remove(removableCollection) != null;
                marvelMoviesDatabaseEntities.SaveChanges();
                return ifAcces;
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Read"/>
        /// </summary>
        /// <returns>COLLECTION list</returns>
        public IEnumerable<COLLECTION> Read()
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                return marvelMoviesDatabaseEntities.COLLECTIONS.ToList();
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Update(T)"/>
        /// </summary>
        /// <param name="obj">COLLECTION object</param>
        public bool Update(COLLECTION obj)
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                var collectionToUpdate = marvelMoviesDatabaseEntities.COLLECTIONS.FirstOrDefault(x => x.COLLID == obj.COLLID);

                if (collectionToUpdate != null)
                {
                    collectionToUpdate.TITLE = obj.TITLE;
                    collectionToUpdate.CREATOR = obj.CREATOR;
                    collectionToUpdate.FIRST_APPEARANCE_YEAR = obj.FIRST_APPEARANCE_YEAR;
                    collectionToUpdate.MOVIE_RELEASE_YEAR = obj.MOVIE_RELEASE_YEAR;
                    collectionToUpdate.MOVIES_COUNT = obj.MOVIES_COUNT;

                    marvelMoviesDatabaseEntities.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
