﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarvelMovies.Data;

namespace MarvelMovies.Repository
{



    /// <summary>
    /// MOVy táblához kapcsolódó CRUD műveleteket megvalósító osztály
    /// <see cref="IRepository{T}"/>
    /// </summary>
    public class MovieRepository : IRepository<MOVy>
    {
        /// <summary>
        /// ??????????????????????
        /// </summary>


        /// <summary>
        /// <see cref="IRepository{T}.Create(T)"/>
        /// </summary>
        /// <param name="obj">T</param>
        /// <returns>MOVID</returns>
        public decimal Create(MOVy obj)
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                marvelMoviesDatabaseEntities.MOVIES.Add(obj);
                marvelMoviesDatabaseEntities.SaveChanges();

                return obj.MOVID;
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Delete(T)"/>
        /// </summary>
        /// <param name="obj">MOVy object</param>
        public bool Delete(decimal id)
        {
            bool ifAcces = false;

            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                var removableMovie = marvelMoviesDatabaseEntities.MOVIES.FirstOrDefault(x => x.MOVID == id);

                if (removableMovie == null)
                {
                    return ifAcces;
                }

                var removableConnectorEntities = marvelMoviesDatabaseEntities.MOVIESCHARACTERSconnectors.Where(x => x.MOVID == id);

                marvelMoviesDatabaseEntities.MOVIESCHARACTERSconnectors.RemoveRange(removableConnectorEntities);

                ifAcces = marvelMoviesDatabaseEntities.MOVIES.Remove(removableMovie) != null;

                marvelMoviesDatabaseEntities.SaveChanges();

                return ifAcces;
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Read"/>
        /// </summary>
        /// <returns>MOVy list</returns>
        public IEnumerable<MOVy> Read()
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                return marvelMoviesDatabaseEntities.MOVIES.ToList();
            }
        }

        /// <summary>
        /// <see cref="IRepository{T}.Update(T)"/>
        /// </summary>
        /// <param name="obj">MOVy object</param>
        public bool Update(MOVy obj)
        {
            using (MarvelMoviesDatabaseEntities marvelMoviesDatabaseEntities = new MarvelMoviesDatabaseEntities())
            {
                var movieToUpdate = marvelMoviesDatabaseEntities.MOVIES.FirstOrDefault(x => x.MOVID == obj.MOVID);

                if (movieToUpdate != null)
                {
                    //movieToUpdate.CRONOLOGICAL_ORDER = obj.CRONOLOGICAL_ORDER;
                    movieToUpdate.MOVIE_NAME = obj.MOVIE_NAME;
                    movieToUpdate.TITLE = obj.TITLE;
                    movieToUpdate.RELEASE_YEAR = obj.RELEASE_YEAR;
                    movieToUpdate.RUNTIME = obj.RUNTIME;
                    movieToUpdate.TOMATOMETER = obj.TOMATOMETER;
                    movieToUpdate.AUDIENCE_SCORE = obj.AUDIENCE_SCORE;
                    //movieToUpdate.COLLID = obj.COLLID;

                    marvelMoviesDatabaseEntities.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
