﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MarvelMovies.ConsoleClient
{
    public class Movie
    {
        public int MOVID { get; set; }
        public int CRONOLOGICAL_ORDER { get; set; }
        public string MOVIE_NAME { get; set; }
        public string TITLE { get; set; }
        public int RELEASE_YEAR { get; set; }
        public int RUNTIME { get; set; }
        public int TOMATOMETER { get; set; }
        public int AUDIENCE_SCORE { get; set; }
        public string CollectionName { get; set; }

        public override string ToString()
        {
            return $"Id={MOVID}\tName={MOVIE_NAME}\tTitle={TITLE}\tReleaseYear={RELEASE_YEAR}\tRuntime={RUNTIME}\tTomatometer={TOMATOMETER}\tAudienceScore={AUDIENCE_SCORE}\t";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("WAITING......");
            Console.ReadLine();

            string url = "http://localhost:53815/api/MoviesApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Movie>>(json);
                foreach (var item in list) Console.WriteLine(item);
                Console.ReadLine();


                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Movie.MOVIE_NAME), "Spider-Man 2");
                postData.Add(nameof(Movie.TITLE), "Spider-Man: Far from Home");
                postData.Add(nameof(Movie.RELEASE_YEAR), "2019");
                postData.Add(nameof(Movie.RUNTIME), "129");
                postData.Add(nameof(Movie.TOMATOMETER), "91");
                postData.Add(nameof(Movie.AUDIENCE_SCORE), "95");
                postData.Add(nameof(Movie.CollectionName), "SPIDER-MAN");

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int movieId = JsonConvert.DeserializeObject<List<Movie>>(json).Single(x => x.MOVIE_NAME == "Spider-Man 2").MOVID;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Movie.MOVID), movieId.ToString());
                postData.Add(nameof(Movie.MOVIE_NAME), "Spider-Man 2");
                postData.Add(nameof(Movie.TITLE), "Spider-Man: Far from Home");
                postData.Add(nameof(Movie.RELEASE_YEAR), "2019");
                postData.Add(nameof(Movie.RUNTIME), "129");
                postData.Add(nameof(Movie.TOMATOMETER), "100");
                postData.Add(nameof(Movie.AUDIENCE_SCORE), "100");
                postData.Add(nameof(Movie.CollectionName), "SPIDER-MAN");

                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + movieId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

            }
        }
    }
}
