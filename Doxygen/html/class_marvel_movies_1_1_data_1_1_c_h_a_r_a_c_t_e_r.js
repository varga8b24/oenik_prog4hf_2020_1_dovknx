var class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r =
[
    [ "CHARACTER", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html#a7d4295e99296a45a6756578fbe3ed8f6", null ],
    [ "ToString", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html#a2931aafa38b330602762e5fc6131852c", null ],
    [ "AFTER_EG_STATUS", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html#a7a5183593bfba99aaabf7a946e26ac44", null ],
    [ "AFTER_IW_STATUS", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html#acf9b6e01527dc7cc11901b974b37d443", null ],
    [ "CHAR_NAME", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html#a940d8f197d2aa8985b81a319a0d3e8ee", null ],
    [ "CHARID", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html#ae96d5a2212949be8209418faf5f61830", null ],
    [ "GOOD_GUY", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html#a8a4148121c5ee5807bd37f2309291a03", null ],
    [ "MOST_SPECIAL_SKILL", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html#aa4582623beaf3fe61ad2f82233ceb91b", null ],
    [ "MOVIESCHARACTERSconnectors", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html#a853656939f759e1b8b4b034fce46ec34", null ],
    [ "NICKNAME", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html#abab11e07ca2c33f64e867083ff63294d", null ],
    [ "ORIGIN", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html#aa5ca1f4a4531be9a74d74b747d4bfb32", null ]
];