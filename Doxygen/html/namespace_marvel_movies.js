var namespace_marvel_movies =
[
    [ "Data", "namespace_marvel_movies_1_1_data.html", "namespace_marvel_movies_1_1_data" ],
    [ "Logic", "namespace_marvel_movies_1_1_logic.html", "namespace_marvel_movies_1_1_logic" ],
    [ "Repository", "namespace_marvel_movies_1_1_repository.html", "namespace_marvel_movies_1_1_repository" ],
    [ "Program", "class_marvel_movies_1_1_program.html", null ]
];