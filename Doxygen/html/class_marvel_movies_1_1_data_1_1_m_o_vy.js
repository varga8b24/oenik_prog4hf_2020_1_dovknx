var class_marvel_movies_1_1_data_1_1_m_o_vy =
[
    [ "MOVy", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#aa40d775268cd2d4ef32e8c00e2c0c36b", null ],
    [ "ToString", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#a4eb6afd1a9f2102beb1aa1e1598ff006", null ],
    [ "AUDIENCE_SCORE", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#a7879575d3c1bf6b63cab394304b38af6", null ],
    [ "COLLECTION", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#aacadc29470aaeb47cc0b3b9444bcbec0", null ],
    [ "COLLID", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#ae17bb231975bbff1c2522fc6757048cc", null ],
    [ "CRONOLOGICAL_ORDER", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#a77876a724132ca69280fc9ef6d2a7562", null ],
    [ "MOVID", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#a1e560c7aa176b06bcee7070bcd18930b", null ],
    [ "MOVIE_NAME", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#aee20b24ae295b5456833864800dae694", null ],
    [ "MOVIESCHARACTERSconnectors", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#a210b09022f62670cc63d54137b7187c1", null ],
    [ "RELEASE_YEAR", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#a348b859dc0ebcaa37d49805858955add", null ],
    [ "RUNTIME", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#a35a21bf69ff5bfa6285361acd6240da6", null ],
    [ "TITLE", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#a0252ef9f977fe7ff13b3fdf84f73d721", null ],
    [ "TOMATOMETER", "class_marvel_movies_1_1_data_1_1_m_o_vy.html#a525e03bcd3112fa77c1d43587a7fb2bd", null ]
];