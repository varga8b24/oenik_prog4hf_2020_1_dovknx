var namespace_marvel_movies_1_1_repository =
[
    [ "CharacterRepository", "class_marvel_movies_1_1_repository_1_1_character_repository.html", "class_marvel_movies_1_1_repository_1_1_character_repository" ],
    [ "CollectionRepository", "class_marvel_movies_1_1_repository_1_1_collection_repository.html", "class_marvel_movies_1_1_repository_1_1_collection_repository" ],
    [ "ConnectorRepository", "class_marvel_movies_1_1_repository_1_1_connector_repository.html", "class_marvel_movies_1_1_repository_1_1_connector_repository" ],
    [ "IRepository", "interface_marvel_movies_1_1_repository_1_1_i_repository.html", "interface_marvel_movies_1_1_repository_1_1_i_repository" ],
    [ "MovieRepository", "class_marvel_movies_1_1_repository_1_1_movie_repository.html", "class_marvel_movies_1_1_repository_1_1_movie_repository" ]
];