var hierarchy =
[
    [ "MarvelMovies.Data.CHARACTER", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html", null ],
    [ "MarvelMovies.Data.COLLECTION", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html", null ],
    [ "DbContext", null, [
      [ "MarvelMovies.Data.MarvelMoviesDatabaseEntities", "class_marvel_movies_1_1_data_1_1_marvel_movies_database_entities.html", null ]
    ] ],
    [ "MarvelMovies.Logic.ILogic", "interface_marvel_movies_1_1_logic_1_1_i_logic.html", [
      [ "MarvelMovies.Logic.MarvelMoviesLogicTool", "class_marvel_movies_1_1_logic_1_1_marvel_movies_logic_tool.html", null ]
    ] ],
    [ "MarvelMovies.Repository.IRepository< T >", "interface_marvel_movies_1_1_repository_1_1_i_repository.html", null ],
    [ "MarvelMovies.Repository.IRepository< CHARACTER >", "interface_marvel_movies_1_1_repository_1_1_i_repository.html", [
      [ "MarvelMovies.Repository.CharacterRepository", "class_marvel_movies_1_1_repository_1_1_character_repository.html", null ]
    ] ],
    [ "MarvelMovies.Repository.IRepository< COLLECTION >", "interface_marvel_movies_1_1_repository_1_1_i_repository.html", [
      [ "MarvelMovies.Repository.CollectionRepository", "class_marvel_movies_1_1_repository_1_1_collection_repository.html", null ]
    ] ],
    [ "MarvelMovies.Repository.IRepository< MOVIESCHARACTERSconnector >", "interface_marvel_movies_1_1_repository_1_1_i_repository.html", [
      [ "MarvelMovies.Repository.ConnectorRepository", "class_marvel_movies_1_1_repository_1_1_connector_repository.html", null ]
    ] ],
    [ "MarvelMovies.Repository.IRepository< MOVy >", "interface_marvel_movies_1_1_repository_1_1_i_repository.html", [
      [ "MarvelMovies.Repository.MovieRepository", "class_marvel_movies_1_1_repository_1_1_movie_repository.html", null ]
    ] ],
    [ "MarvelMovies.Logic.Tests.MarvelMoviesTests", "class_marvel_movies_1_1_logic_1_1_tests_1_1_marvel_movies_tests.html", null ],
    [ "MarvelMovies.Data.MOVIESCHARACTERSconnector", "class_marvel_movies_1_1_data_1_1_m_o_v_i_e_s_c_h_a_r_a_c_t_e_r_sconnector.html", null ],
    [ "MarvelMovies.Data.MOVy", "class_marvel_movies_1_1_data_1_1_m_o_vy.html", null ],
    [ "MarvelMovies.Program", "class_marvel_movies_1_1_program.html", null ]
];