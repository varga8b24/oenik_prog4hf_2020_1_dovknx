var dir_05c6bc1afc9b7196326b5f35eefd19cc =
[
    [ "MarvelMovies", "dir_3414a8a49b5193784e96a297774d246c.html", "dir_3414a8a49b5193784e96a297774d246c" ],
    [ "MarvelMovies.Data", "dir_9b6db8480612162c7506eb7f1b02a541.html", "dir_9b6db8480612162c7506eb7f1b02a541" ],
    [ "MarvelMovies.Logic", "dir_88f71dd575c78a31002d1f93b23dff51.html", "dir_88f71dd575c78a31002d1f93b23dff51" ],
    [ "MarvelMovies.Logic.Tests", "dir_333880385f1a5fe76cda83572bbeb220.html", "dir_333880385f1a5fe76cda83572bbeb220" ],
    [ "MarvelMovies.Repository", "dir_053bc41c876e6a7630818af5befecdb3.html", "dir_053bc41c876e6a7630818af5befecdb3" ]
];