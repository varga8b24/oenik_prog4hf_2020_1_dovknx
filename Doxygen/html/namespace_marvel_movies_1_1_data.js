var namespace_marvel_movies_1_1_data =
[
    [ "CHARACTER", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html", "class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r" ],
    [ "COLLECTION", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n" ],
    [ "MarvelMoviesDatabaseEntities", "class_marvel_movies_1_1_data_1_1_marvel_movies_database_entities.html", "class_marvel_movies_1_1_data_1_1_marvel_movies_database_entities" ],
    [ "MOVIESCHARACTERSconnector", "class_marvel_movies_1_1_data_1_1_m_o_v_i_e_s_c_h_a_r_a_c_t_e_r_sconnector.html", "class_marvel_movies_1_1_data_1_1_m_o_v_i_e_s_c_h_a_r_a_c_t_e_r_sconnector" ],
    [ "MOVy", "class_marvel_movies_1_1_data_1_1_m_o_vy.html", "class_marvel_movies_1_1_data_1_1_m_o_vy" ]
];