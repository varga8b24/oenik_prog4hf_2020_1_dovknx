var class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n =
[
    [ "COLLECTION", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html#a630ea000c882a813466512680d8f518c", null ],
    [ "ToString", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html#a920aa06bfa76edd92d21805bde8abe84", null ],
    [ "COLLID", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html#a10f105a98a2870283402d7ffbe423a60", null ],
    [ "CREATOR", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html#a9064827488effd0cdb062dac47bfc356", null ],
    [ "FIRST_APPEARANCE_YEAR", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html#a4d5cad69106b69d06130ccdc16f4198c", null ],
    [ "MOVIE_RELEASE_YEAR", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html#adf064c0da1bbe5876766b99430d1c0ca", null ],
    [ "MOVIES", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html#a4191ec22300fa8ca84dcf7d175b47847", null ],
    [ "MOVIES_COUNT", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html#ac9c69e252fe3e143944f7b6551e1395e", null ],
    [ "TITLE", "class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html#a59c620be524ef355cdd2187169c0e24b", null ]
];