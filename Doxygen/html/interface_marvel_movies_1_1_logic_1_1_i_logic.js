var interface_marvel_movies_1_1_logic_1_1_i_logic =
[
    [ "AddCharacter", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#a1c047127064f3ef90b932c4d08e8b60a", null ],
    [ "AddCollection", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#af77a2781f14685897436b012f0b4b532", null ],
    [ "AddMovie", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#a9b390b0828486853bcc8db888481de94", null ],
    [ "DeleteCharacter", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#a924b8452208b9209880b6d5fd5e62fb8", null ],
    [ "DeleteCollection", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#a0f99f6c7b73003821e805dcea9a812d7", null ],
    [ "DeleteMovie", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#a37b042dbfa52e8bd45fa87f1eda43ea5", null ],
    [ "GetCharactersMoviesandInfoPage", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#a66c7e4a6a4884270442d4fa7d9caeb32", null ],
    [ "GetMovieWithTheMostCharactersInIt", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#a727fe4b27627a3c77767a9cff1d4cc2d", null ],
    [ "GetTop3CharacterWithMostAppearance", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#a7198e14ecd35c61cb451d9222f88aa76", null ],
    [ "GetTop3MoviesByAveragedRating", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#af90807136c2e1b09c1f8873c2cf3914b", null ],
    [ "ListCharacters", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#ae0b39cdeebc692b10b0a0c1e316398b7", null ],
    [ "ListCollections", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#ac1a71ea93ae0bb6546325fe5a7855c11", null ],
    [ "ListMovies", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#a3d71e943d7b176bd67a266251d731077", null ],
    [ "UpdateCharacter", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#ada54c4b2f26a3744dbf7cbf8567951f4", null ],
    [ "UpdateCollection", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#af0ebfad0f9e130496bc33795ce016c2e", null ],
    [ "UpdateMovie", "interface_marvel_movies_1_1_logic_1_1_i_logic.html#aebf4e0e99abb6346ec12cbd7c51b6011", null ]
];