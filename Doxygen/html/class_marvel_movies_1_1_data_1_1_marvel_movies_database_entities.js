var class_marvel_movies_1_1_data_1_1_marvel_movies_database_entities =
[
    [ "MarvelMoviesDatabaseEntities", "class_marvel_movies_1_1_data_1_1_marvel_movies_database_entities.html#a8c61eeb7e270b3f706a8586d648a0b06", null ],
    [ "OnModelCreating", "class_marvel_movies_1_1_data_1_1_marvel_movies_database_entities.html#a9ba0142c57da701b20256784fb9862b8", null ],
    [ "CHARACTERS", "class_marvel_movies_1_1_data_1_1_marvel_movies_database_entities.html#a73a225954ab7c0b3cd7563c5a0a7f734", null ],
    [ "COLLECTIONS", "class_marvel_movies_1_1_data_1_1_marvel_movies_database_entities.html#a592a5dc7b126dcd7ca0d1ca11d089af6", null ],
    [ "MOVIES", "class_marvel_movies_1_1_data_1_1_marvel_movies_database_entities.html#a1acb9563cfe0d58644d0d991f5ea2412", null ],
    [ "MOVIESCHARACTERSconnectors", "class_marvel_movies_1_1_data_1_1_marvel_movies_database_entities.html#a5d3b21bd8bdbf4456aa0ed1f48af2907", null ]
];