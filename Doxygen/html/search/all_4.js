var searchData=
[
  ['ilogic',['ILogic',['../interface_marvel_movies_1_1_logic_1_1_i_logic.html',1,'MarvelMovies::Logic']]],
  ['insertjavaendpointoutputtodb',['InsertJavaEndPointOutputToDb',['../class_marvel_movies_1_1_logic_1_1_marvel_movies_logic_tool.html#a000b70bb7f3c66d1f663472d83e876af',1,'MarvelMovies::Logic::MarvelMoviesLogicTool']]],
  ['irepository',['IRepository',['../interface_marvel_movies_1_1_repository_1_1_i_repository.html',1,'MarvelMovies::Repository']]],
  ['irepository_3c_20character_20_3e',['IRepository&lt; CHARACTER &gt;',['../interface_marvel_movies_1_1_repository_1_1_i_repository.html',1,'MarvelMovies::Repository']]],
  ['irepository_3c_20collection_20_3e',['IRepository&lt; COLLECTION &gt;',['../interface_marvel_movies_1_1_repository_1_1_i_repository.html',1,'MarvelMovies::Repository']]],
  ['irepository_3c_20moviescharactersconnector_20_3e',['IRepository&lt; MOVIESCHARACTERSconnector &gt;',['../interface_marvel_movies_1_1_repository_1_1_i_repository.html',1,'MarvelMovies::Repository']]],
  ['irepository_3c_20movy_20_3e',['IRepository&lt; MOVy &gt;',['../interface_marvel_movies_1_1_repository_1_1_i_repository.html',1,'MarvelMovies::Repository']]]
];
