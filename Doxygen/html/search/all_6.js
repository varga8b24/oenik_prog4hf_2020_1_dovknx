var searchData=
[
  ['data',['Data',['../namespace_marvel_movies_1_1_data.html',1,'MarvelMovies']]],
  ['logic',['Logic',['../namespace_marvel_movies_1_1_logic.html',1,'MarvelMovies']]],
  ['marvelmovies',['MarvelMovies',['../namespace_marvel_movies.html',1,'']]],
  ['marvelmoviesdatabaseentities',['MarvelMoviesDatabaseEntities',['../class_marvel_movies_1_1_data_1_1_marvel_movies_database_entities.html',1,'MarvelMovies::Data']]],
  ['marvelmovieslogictool',['MarvelMoviesLogicTool',['../class_marvel_movies_1_1_logic_1_1_marvel_movies_logic_tool.html',1,'MarvelMovies.Logic.MarvelMoviesLogicTool'],['../class_marvel_movies_1_1_logic_1_1_marvel_movies_logic_tool.html#a9148e33d86b0048c089eab7b02fbb6dc',1,'MarvelMovies.Logic.MarvelMoviesLogicTool.MarvelMoviesLogicTool(IRepository&lt; CHARACTER &gt; characterRepository)'],['../class_marvel_movies_1_1_logic_1_1_marvel_movies_logic_tool.html#a2a40d3585d6c79bbc72c87b3cf0a3aac',1,'MarvelMovies.Logic.MarvelMoviesLogicTool.MarvelMoviesLogicTool(IRepository&lt; COLLECTION &gt; collectionRepository)'],['../class_marvel_movies_1_1_logic_1_1_marvel_movies_logic_tool.html#a6326a793926ab16e06737d1363e60bc2',1,'MarvelMovies.Logic.MarvelMoviesLogicTool.MarvelMoviesLogicTool(IRepository&lt; MOVy &gt; movieRepository)'],['../class_marvel_movies_1_1_logic_1_1_marvel_movies_logic_tool.html#a01ca35c1b5faf7bb4af2e8e9df959112',1,'MarvelMovies.Logic.MarvelMoviesLogicTool.MarvelMoviesLogicTool(IRepository&lt; MOVIESCHARACTERSconnector &gt; connectorRepository)']]],
  ['marvelmoviestests',['MarvelMoviesTests',['../class_marvel_movies_1_1_logic_1_1_tests_1_1_marvel_movies_tests.html',1,'MarvelMovies::Logic::Tests']]],
  ['movierepository',['MovieRepository',['../class_marvel_movies_1_1_repository_1_1_movie_repository.html',1,'MarvelMovies::Repository']]],
  ['moviescharactersconnector',['MOVIESCHARACTERSconnector',['../class_marvel_movies_1_1_data_1_1_m_o_v_i_e_s_c_h_a_r_a_c_t_e_r_sconnector.html',1,'MarvelMovies::Data']]],
  ['moviescrudmock',['MoviesCRUDmock',['../class_marvel_movies_1_1_logic_1_1_tests_1_1_marvel_movies_tests.html#a61122dab51644d50ee96cfff0f072f56',1,'MarvelMovies::Logic::Tests::MarvelMoviesTests']]],
  ['movy',['MOVy',['../class_marvel_movies_1_1_data_1_1_m_o_vy.html',1,'MarvelMovies::Data']]],
  ['repository',['Repository',['../namespace_marvel_movies_1_1_repository.html',1,'MarvelMovies']]],
  ['tests',['Tests',['../namespace_marvel_movies_1_1_logic_1_1_tests.html',1,'MarvelMovies::Logic']]]
];
