var searchData=
[
  ['character',['CHARACTER',['../class_marvel_movies_1_1_data_1_1_c_h_a_r_a_c_t_e_r.html',1,'MarvelMovies::Data']]],
  ['charactercrudmock',['CharacterCRUDmock',['../class_marvel_movies_1_1_logic_1_1_tests_1_1_marvel_movies_tests.html#a989d8d585dcd0905f1c8cde6467e9293',1,'MarvelMovies::Logic::Tests::MarvelMoviesTests']]],
  ['characterrepository',['CharacterRepository',['../class_marvel_movies_1_1_repository_1_1_character_repository.html',1,'MarvelMovies::Repository']]],
  ['collection',['COLLECTION',['../class_marvel_movies_1_1_data_1_1_c_o_l_l_e_c_t_i_o_n.html',1,'MarvelMovies::Data']]],
  ['collectionrepository',['CollectionRepository',['../class_marvel_movies_1_1_repository_1_1_collection_repository.html',1,'MarvelMovies::Repository']]],
  ['collectionscrudmock',['CollectionsCRUDmock',['../class_marvel_movies_1_1_logic_1_1_tests_1_1_marvel_movies_tests.html#a236dcc62304bf2d80bb8481e4bd04623',1,'MarvelMovies::Logic::Tests::MarvelMoviesTests']]],
  ['connectorrepository',['ConnectorRepository',['../class_marvel_movies_1_1_repository_1_1_connector_repository.html',1,'MarvelMovies::Repository']]],
  ['create',['Create',['../class_marvel_movies_1_1_repository_1_1_character_repository.html#a2cd7ce6a91ba67d32a31ae934234ecc1',1,'MarvelMovies.Repository.CharacterRepository.Create()'],['../class_marvel_movies_1_1_repository_1_1_collection_repository.html#a573a986831a4773585bd91d2a138ed67',1,'MarvelMovies.Repository.CollectionRepository.Create()'],['../class_marvel_movies_1_1_repository_1_1_connector_repository.html#abdd06b3f55a59f6953cd263eb60824ee',1,'MarvelMovies.Repository.ConnectorRepository.Create()'],['../interface_marvel_movies_1_1_repository_1_1_i_repository.html#a0a96a47154932c05d2931647ca5914f7',1,'MarvelMovies.Repository.IRepository.Create()'],['../class_marvel_movies_1_1_repository_1_1_movie_repository.html#aeccddb997b5c600fe90ff3061dae07bd',1,'MarvelMovies.Repository.MovieRepository.Create()']]],
  ['castle_20core_20changelog',['Castle Core Changelog',['../md__c_1__users_alias__documents__visual__studio_2017__projects_oenik_prog3_2019_1_dovknx__c_s_h_434e8d8775831d93fa1beb2ed88072ea.html',1,'']]]
];
