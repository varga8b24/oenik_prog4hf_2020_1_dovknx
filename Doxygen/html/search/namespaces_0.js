var searchData=
[
  ['data',['Data',['../namespace_marvel_movies_1_1_data.html',1,'MarvelMovies']]],
  ['logic',['Logic',['../namespace_marvel_movies_1_1_logic.html',1,'MarvelMovies']]],
  ['marvelmovies',['MarvelMovies',['../namespace_marvel_movies.html',1,'']]],
  ['repository',['Repository',['../namespace_marvel_movies_1_1_repository.html',1,'MarvelMovies']]],
  ['tests',['Tests',['../namespace_marvel_movies_1_1_logic_1_1_tests.html',1,'MarvelMovies::Logic']]]
];
