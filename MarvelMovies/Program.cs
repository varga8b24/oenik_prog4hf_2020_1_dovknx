﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using MarvelMovies.Data;
using MarvelMovies.Logic;


namespace MarvelMovies
{
    /// <summary>
    /// Kiegészítő metódusok
    /// </summary>
    static class EnumerableExtensions
    {
        /// <summary>
        /// Lekérdezések eredményének kiiratása Console-ra
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="header"></param>
        public static void ToConsole<T>(this IEnumerable<T> collection, string header)
        {
            Console.WriteLine($"---------------- BEGIN ### {header} ### ----------------");
            foreach (var item in collection)
            {
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine($"----------------- END ### {header} ### -----------------");
            Console.WriteLine();
        }
    }

    class Program
    {
        /// <summary>
        /// Console-on kiválasztott menu száma (amit a felhasználó beír)
        /// </summary>
        private static int menuIndex = 0;

        /// <summary>
        /// LogicTool példánya
        /// </summary>
        static MarvelMoviesLogicTool logicTool = new MarvelMoviesLogicTool();

        /// <summary>
        /// Program belépési pont
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            while (menuIndex < 18)
            {
                Process();
            }

            Environment.Exit(0);
        }

        /// <summary>
        /// Menu kirajzolása Console-ra
        /// </summary>
        static void DrawMenu()
        {
            Console.Clear();

            Console.WriteLine("1.) List Collections");
            Console.WriteLine("2.) List Movies (THE REAL CHRONOLOGICAL ORDER)");
            Console.WriteLine("3.) List Characters");
            Console.WriteLine();
            Console.WriteLine("4.) Add Collection");
            Console.WriteLine("5.) Add Movie");
            Console.WriteLine("6.) Add Character");
            Console.WriteLine();
            Console.WriteLine("7.) Update Collection");
            Console.WriteLine("8.) Update Movie");
            Console.WriteLine("9.) Update Character");
            Console.WriteLine();
            Console.WriteLine("10.) Delete Collection");
            Console.WriteLine("11.) Delete Movie");
            Console.WriteLine("12.) Delete Character");
            Console.WriteLine();
            Console.WriteLine("13.) Get the Movie with the most character in it :)");
            Console.WriteLine();
            Console.WriteLine("14.) Get the TOP3 CHARACTER with the best avaraged rated Movie (by RottenTomatoes)");
            Console.WriteLine();
            Console.WriteLine("15.) Get the TOP3 Character with the most apperance in Movies");
            Console.WriteLine();
            Console.WriteLine("16.) Type in a character's name & get his/her info page (movies with, character infos)");
            Console.WriteLine();
            Console.WriteLine("17.) Connect to Java EndPoint...");
            Console.WriteLine();
            Console.WriteLine("18.) Exit");
            Console.WriteLine();
        }

        static void DoDBWork(int menuIndex)
        {
            #region DONE
            if (menuIndex == 1)//1.) List Collections
            {
                var result = logicTool.ListCollections();
                result.ToConsole("LIST COLLECTIONS");
            }
            else if (menuIndex == 2)//2.) List Movies (THE REAL CHRONOLOGICAL ORDER)"
            {
                var result = logicTool.ListMovies();
                result.ToConsole("LIST MOVIES");
            }
            else if (menuIndex == 3)//3.) List Characters
            {
                var result = logicTool.ListCharacters();
                result.ToConsole("LIST CHARACTERS");
            }
            else if (menuIndex == 4)//4.) Add Collection
            {
                COLLECTION newCollection = new COLLECTION
                {
                    TITLE = "BLACK WIDOW",
                    CREATOR = "Stan Lee",
                    FIRST_APPEARANCE_YEAR = 1964,
                    MOVIE_RELEASE_YEAR = 2020,
                    MOVIES_COUNT = 1
                };

                logicTool.AddCollection(newCollection);

                Console.WriteLine("COLLECTION ADDED!: " + newCollection.TITLE);
            }
            else if (menuIndex == 5)//5.) Add Movie
            {
                MOVy newMovie = new MOVy
                {
                    CRONOLOGICAL_ORDER = 99,
                    MOVIE_NAME = "Spider-Man 2",
                    TITLE = "Spider-Man: Far from Home",
                    RELEASE_YEAR = 2019,
                    RUNTIME = 150,
                    TOMATOMETER = 100,
                    AUDIENCE_SCORE = 100,
                    COLLID = 9
                };

                logicTool.AddMovie(newMovie);

                Console.WriteLine("MOVIE ADDED!: " + newMovie.MOVIE_NAME);
            }
            else if (menuIndex == 6)//6.) Add Character
            {
                CHARACTER newCharacter = new CHARACTER
                {
                    CHAR_NAME = "Quentin Beck",
                    NICKNAME = "Mysterio",
                    ORIGIN = "human",
                    MOST_SPECIAL_SKILL = "illusion",
                    GOOD_GUY = true,
                    AFTER_IW_STATUS = null,
                    AFTER_EG_STATUS = true
                };

                logicTool.AddCharacter(newCharacter);

                Console.WriteLine("CHARACTER ADDED!: " + newCharacter.CHAR_NAME);
            }
            else if (menuIndex == 7)//7.) Update Collection
            {
                Console.WriteLine("WHICH COLLECTION YOU WOULD LIKE TO UPDATE?");
                string collectionToUpdate = Console.ReadLine().ToLower();

                COLLECTION foundCollectionToUpdate = logicTool.ListCollections().Where(x => x.TITLE.ToLower() == collectionToUpdate).FirstOrDefault();
                //UPDATE SG!!!

                logicTool.UpdateCollection(foundCollectionToUpdate);
                Console.WriteLine("COLLECTION UPDATED!" + foundCollectionToUpdate.ToString());

            }
            else if (menuIndex == 8)//8.) Update Movie
            {
                Console.WriteLine("WHICH MOVIE YOU WOULD LIKE TO UPDATE?");
                string movieToUpdate = Console.ReadLine().ToLower();

                MOVy foundMovieToUpdate = logicTool.ListMovies().Where(x => x.MOVIE_NAME.ToLower() == movieToUpdate || x.TITLE.ToLower() == movieToUpdate).FirstOrDefault();
                //UPDATE SG!!!

                logicTool.UpdateMovie(foundMovieToUpdate);
                Console.WriteLine("MOVIE UPDATED!" + foundMovieToUpdate.ToString());
            }
            else if (menuIndex == 9)//9.) Update Character
            {
                Console.WriteLine("WHICH CHARACTER YOU WOULD LIKE TO UPDATE?");
                string characterToUpdate = Console.ReadLine().ToLower();

                CHARACTER foundCharacterToUpdate = logicTool.ListCharacters().Where(x => x.CHAR_NAME.ToLower() == characterToUpdate || x.NICKNAME.ToLower() == characterToUpdate).FirstOrDefault();
                foundCharacterToUpdate.AFTER_EG_STATUS = true;

                logicTool.UpdateCharacter(foundCharacterToUpdate);
                Console.WriteLine("CHARACTER UPDATED!" + foundCharacterToUpdate.ToString());
            }
            else if (menuIndex == 10)//10.) Delete Collection
            {
                Console.WriteLine("WHICH COLLECTION YOU WOULD LIKE TO DELETE?");
                string collectionToDelete = Console.ReadLine().ToLower();

                COLLECTION foundCollectionToDelete = logicTool.ListCollections().Where(x => x.TITLE.ToLower() == collectionToDelete).FirstOrDefault();

                logicTool.DeleteCollection(foundCollectionToDelete.COLLID);
                Console.WriteLine("COLLECTION DELETED...");
            }
            else if (menuIndex == 11)//11.) Delete Movie
            {
                Console.WriteLine("WHICH MOVIE YOU WOULD LIKE TO DELETE?");
                string movieToDelete = Console.ReadLine().ToLower();

                MOVy foundMovieToDelete = logicTool.ListMovies().Where(x => x.MOVIE_NAME.ToLower() == movieToDelete || x.TITLE.ToLower() == movieToDelete).FirstOrDefault();

                logicTool.DeleteMovie(foundMovieToDelete.COLLID);
                Console.WriteLine("MOVIE DELETED...");
            }
            else if (menuIndex == 12)//12.) Delete Character
            {
                Console.WriteLine("WHICH CHARACTER YOU WOULD LIKE TO DELETE?");
                string characterToDelete = Console.ReadLine().ToLower();

                CHARACTER foundCharacterToDelete = logicTool.ListCharacters().Where(x => x.CHAR_NAME.ToLower() == characterToDelete || x.NICKNAME.ToLower() == characterToDelete).FirstOrDefault();

                logicTool.DeleteCharacter(foundCharacterToDelete.CHARID);
                Console.WriteLine("CHARACTER DELETED...");
            }
            #endregion
            else if (menuIndex == 13)//13.) Get the Movie with the most character in it :)
            {
                MOVy movieWithTheMostCharacterInIt = logicTool.GetMovieWithTheMostCharactersInIt();

                Console.WriteLine(movieWithTheMostCharacterInIt);
            }
            else if (menuIndex == 14)//14.) Get the TOP3 best avaraged rated Movie (by RottenTomatoes)
            {
                IEnumerable<MOVy> top3BestRatedMovie = logicTool.GetTop3MoviesByAveragedRating();

                top3BestRatedMovie.ToConsole("Get the TOP3 best avaraged rated Movie (by RottenTomatoes)");
            }
            else if (menuIndex == 15)//15.) Get the TOP3 Character with the most apperance in Movies
            {
                IEnumerable<CHARACTER> top3charactersWithTheMostApperanceInMovies = logicTool.GetTop3CharacterWithMostAppearance();

                top3charactersWithTheMostApperanceInMovies.ToConsole("Get the TOP3 Character with the most apperance in Movies");
            }
            else if (menuIndex == 16)//16.) Type in a character's name & get his/her info page (movies with, character infos)
            {
                Console.WriteLine("WHOSE MOVIES and INFO PAGE ARE YOU INTERESTED IN?");
                IEnumerable<MOVy> characterInfo = logicTool.GetCharactersMoviesandInfoPage(Console.ReadLine().ToLower());
                characterInfo.ToConsole("MOVIE WITH THIS CHARACTER IN IT");
            }
            else if (menuIndex == 17)
            {
                Console.WriteLine("WHAT IS YOUR JAVA ENDPOINT's PORT NUMBER?");
                Console.WriteLine("TYPE IT HERE FOR THE CONNECTION: ");
                string javaEndpointPort = Console.ReadLine();

                string url = $@"http://localhost:{javaEndpointPort}/JavaEndPoint/AllCollections";

                logicTool.InsertJavaEndPointOutputToDb(url);
            }

            else if (menuIndex == 18)
            {
                return;
            }
            else
            {
                Console.WriteLine("Nem megfelelő menüelemet választott. Nem létezik funkcionalitás! Kérem válasszon a felsorolt menüelemek közül!");
            }

            Console.WriteLine("A folytatáshoz nyomjon le egy billentyűt!");
            Console.ReadKey();
        }

        /// <summary>
        /// Ismétlődő művelet a menu kirajzolására és az ott kiválasztott elemnek megfelelő kódrészlet lefuttatásásra
        /// </summary>
        static void Process()
        {
            DrawMenu();

            try
            {
                menuIndex = int.Parse(Console.ReadLine());

                DoDBWork(menuIndex);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
