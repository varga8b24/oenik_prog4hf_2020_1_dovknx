﻿using MarvelMovies.Data;
using MarvelMovies.Repository;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarvelMovies.Logic.Tests
{
    public class MarvelMoviesTests
    {
        /// <summary>
        /// CHARACTER tábla CRUD műveleteinek tesztelése
        /// </summary>
        [TestCase]
        public void CharacterCRUDmock()
        {
            var characterMock = new Moq.Mock<IRepository<CHARACTER>>();

            characterMock.Setup(x => x.Create(Moq.It.IsAny<CHARACTER>())).Returns(1);
            characterMock.Setup(x => x.Read()).Returns(new List<CHARACTER>());
            characterMock.Setup(x => x.Update(Moq.It.IsAny<CHARACTER>()));
            characterMock.Setup(x => x.Delete(Moq.It.IsAny<decimal>()));

            var logicTool = new MarvelMoviesLogicTool(characterMock.Object);

            decimal createReturValue = logicTool.AddCharacter(new CHARACTER());
            IEnumerable<CHARACTER> readReturnValue = logicTool.ListCharacters();
            logicTool.UpdateCharacter(new CHARACTER());
            logicTool.DeleteCharacter(readReturnValue.FirstOrDefault()?.CHARID ?? 0);

            characterMock.VerifyAll();

            Assert.AreEqual(1, createReturValue);
            Assert.IsNotNull(readReturnValue);
        }

        /// <summary>
        /// MOVy tábla CRUD műveleteinek tesztelése
        /// </summary>
        [TestCase]
        public void MoviesCRUDmock()
        {
            var moviesMock = new Moq.Mock<IRepository<MOVy>>();

            moviesMock.Setup(x => x.Create(Moq.It.IsAny<MOVy>())).Returns(1);
            moviesMock.Setup(x => x.Read()).Returns(new List<MOVy>());
            moviesMock.Setup(x => x.Update(Moq.It.IsAny<MOVy>()));
            moviesMock.Setup(x => x.Delete(Moq.It.IsAny<decimal>()));

            var logicTool = new MarvelMoviesLogicTool(moviesMock.Object);

            decimal createReturValue = logicTool.AddMovie(new MOVy());
            IEnumerable<MOVy> readReturnValue = logicTool.ListMovies();
            logicTool.UpdateMovie(new MOVy());
            logicTool.DeleteMovie(readReturnValue.FirstOrDefault()?.COLLID ?? 0);

            moviesMock.VerifyAll();

            Assert.AreEqual(1, createReturValue);
            Assert.IsNotNull(readReturnValue);
        }

        /// <summary>
        /// COLLECTION tábla CRUD műveleteinek tesztelése
        /// </summary>
        [TestCase]
        public void CollectionsCRUDmock()
        {
            var collectionsMock = new Moq.Mock<IRepository<COLLECTION>>();

            collectionsMock.Setup(x => x.Create(Moq.It.IsAny<COLLECTION>())).Returns(1);
            collectionsMock.Setup(x => x.Read()).Returns(new List<COLLECTION>());
            collectionsMock.Setup(x => x.Update(Moq.It.IsAny<COLLECTION>()));
            collectionsMock.Setup(x => x.Delete(Moq.It.IsAny<decimal>()));

            var logicTool = new MarvelMoviesLogicTool(collectionsMock.Object);

            decimal createReturValue = logicTool.AddCollection(new COLLECTION());
            IEnumerable<COLLECTION> readReturnValue = logicTool.ListCollections();
            logicTool.UpdateCollection(new COLLECTION());
            logicTool.DeleteCollection(readReturnValue.FirstOrDefault()?.COLLID ?? 0);

            collectionsMock.VerifyAll();

            Assert.AreEqual(1, createReturValue);
            Assert.IsNotNull(readReturnValue);
        }

        /// <summary>
        /// Nem CRUD műveletek tesztelése
        /// </summary>
        [TestCase]
        public void NotCRUDmock()
        {
            var collectionsMock = new Moq.Mock<IRepository<COLLECTION>>();
            var moviesMock = new Moq.Mock<IRepository<MOVy>>();
            var characterMock = new Moq.Mock<IRepository<CHARACTER>>();
            var connectorMock = new Moq.Mock<IRepository<MOVIESCHARACTERSconnector>>();

            characterMock.Setup(x => x.Read()).Returns(new List<CHARACTER>());
            moviesMock.Setup(x => x.Read()).Returns(new List<MOVy>());
            connectorMock.Setup(x => x.Read()).Returns(new List<MOVIESCHARACTERSconnector>());

            var logicTool = new MarvelMoviesLogicTool(characterMock.Object, collectionsMock.Object, moviesMock.Object, connectorMock.Object);

            var result = logicTool.GetCharactersMoviesandInfoPage("iron man");
            var result1 = logicTool.GetMovieWithTheMostCharactersInIt();
            var result2 = logicTool.GetTop3CharacterWithMostAppearance();
            var result3 = logicTool.GetTop3MoviesByAveragedRating();

            moviesMock.Verify(x => x.Read());
            characterMock.Verify(x => x.Read());
            connectorMock.Verify(x => x.Read());
        }
    }
}
