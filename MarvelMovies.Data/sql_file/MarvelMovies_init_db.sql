﻿
IF OBJECT_ID('MOVIESCHARACTERSconnector', 'U') IS NOT NULL 
BEGIN
TRUNCATE TABLE MOVIESCHARACTERSconnector
DROP TABLE MOVIESCHARACTERSconnector;
END
IF OBJECT_ID('MOVIES', 'U') IS NOT NULL 
BEGIN
TRUNCATE TABLE MOVIES
DROP TABLE MOVIES;
END
IF OBJECT_ID('COLLECTIONS', 'U') IS NOT NULL 
BEGIN
TRUNCATE TABLE COLLECTIONS
DROP TABLE COLLECTIONS;
END
IF OBJECT_ID('CHARACTERS', 'U') IS NOT NULL
BEGIN
TRUNCATE TABLE CHARACTERS
DROP TABLE CHARACTERS;
END

CREATE TABLE COLLECTIONS 
    (COLLID              	NUMERIC(2) IDENTITY(1, 1) NOT NULL,
     TITLE               	VARCHAR(100),
     CREATOR               	VARCHAR(20),
	 FIRST_APPEARANCE_YEAR	NUMERIC(4),
	 MOVIE_RELEASE_YEAR		NUMERIC(4),
	 MOVIES_COUNT			NUMERIC(1),	
  CONSTRAINT COLLECTIONS_PRIMARY_KEY PRIMARY KEY (COLLID));

INSERT INTO COLLECTIONS VALUES ('IRON MAN','Stan Lee',1963,2008,3);
INSERT INTO COLLECTIONS VALUES ('HULK','Stan Lee',1962,2008,1);
INSERT INTO COLLECTIONS VALUES ('THOR','Stan Lee',1962,2011,3);
INSERT INTO COLLECTIONS VALUES ('CAPTAIN AMERICA','Joe Simon',1941,2011,3);
INSERT INTO COLLECTIONS VALUES ('AVENGERS','Stan Lee',1963,2012,3);
INSERT INTO COLLECTIONS VALUES ('GUARDIANS OF THE GALAXY','Stan Lee',1969,2014,2);
INSERT INTO COLLECTIONS VALUES ('ANT-MAN','Stan Lee',1962,2015,2);
INSERT INTO COLLECTIONS VALUES ('DOCTOR STRANGE','Stan Lee',1963,2016,1);
INSERT INTO COLLECTIONS VALUES ('SPIDER-MAN','Stan Lee',1962,2017,1);
INSERT INTO COLLECTIONS VALUES ('BLACK PANTHER','Stan Lee',1966,2018,1);
INSERT INTO COLLECTIONS VALUES ('CAPTAIN MARVEL','Stan Lee',1963,2019,1);




CREATE TABLE MOVIES 
    (MOVID               	NUMERIC(2)  IDENTITY(1, 1) NOT NULL,
	 CRONOLOGICAL_ORDER		NUMERIC(2),
     MOVIE_NAME		        VARCHAR(30),
     TITLE		            VARCHAR(40),
     RELEASE_YEAR           NUMERIC(4),
     RUNTIME		        NUMERIC(3),
     TOMATOMETER			NUMERIC(3),
	 AUDIENCE_SCORE			NUMERIC(3),
     COLLID              	NUMERIC(2) NOT NULL,
  CONSTRAINT MOVIES_FOREIGN_KEY FOREIGN KEY (COLLID) REFERENCES COLLECTIONS (COLLID),
  CONSTRAINT MOVIES_PRIMARY_KEY PRIMARY KEY (MOVID));

INSERT INTO MOVIES VALUES (3,'Iron Man 1','Iron Man 1',2008,121,93,91,1);
INSERT INTO MOVIES VALUES (4,'Iron Man 2','Iron Man 2',2010,124,73,71,1);
INSERT INTO MOVIES VALUES (8,'Iron Man 3','Iron Man 3',2013,109,80,78,1);
INSERT INTO MOVIES VALUES (5,'Hulk 1','The Incredible Hulk',2008,109,67,70,2);
INSERT INTO MOVIES VALUES (6,'Thor 1','Thor',2011,115,77,76,3);
INSERT INTO MOVIES VALUES (9,'Thor 2','Thor: The Dark World',2013,112,67,76,3);
INSERT INTO MOVIES VALUES (19,'Thor 3','Thor: Ragnarok',2017,130,92,87,3);
INSERT INTO MOVIES VALUES (1,'Captain America 1','Captain America: The First Avenger',2011,124,80,74,4);
INSERT INTO MOVIES VALUES (10,'Captain America 2','Captain America: The Winter Soldier',2014,136,90,92,4);
INSERT INTO MOVIES VALUES (15,'Captain America 3','Captain America: Civil War',2016,146,91,89,4);
INSERT INTO MOVIES VALUES (7,'Avengers 1','Marvels The Avengers',2012,142,92,91,5);
INSERT INTO MOVIES VALUES (13,'Avengers 2','Avengers: Age of Ultron',2015,141,75,83,5);
INSERT INTO MOVIES VALUES (21,'Avengers 3','Avengers: Infinity War',2018,156,85,91,5);
INSERT INTO MOVIES VALUES (22,'Avengers 4','Avengers: Endgame',2019,180,100,100,5);
INSERT INTO MOVIES VALUES (11,'Guardians of the Galaxy 1','Guardians of the Galaxy',2014,121,91,92,6);
INSERT INTO MOVIES VALUES (12,'Guardians of the Galaxy 2','Guardians of the Galaxy Vol.2',2017,137,84,87,6);
INSERT INTO MOVIES VALUES (14,'Ant-Man 1','Ant-Man',2015,137,82,86,7);
INSERT INTO MOVIES VALUES (20,'Ant-Man 2','Ant-Man and the Wasp',2018,118,88,77,7);
INSERT INTO MOVIES VALUES (16,'Doctor Strange 1','Doctor Strange',2016,130,89,86,8);
INSERT INTO MOVIES VALUES (18,'Spider-Man 1','Spider-Man: Homecoming',2017,133,92,88,9);
INSERT INTO MOVIES VALUES (17,'Black Panther 1','Black Panther',2018,135,97,79,9);
INSERT INTO MOVIES VALUES (2,'Captain Marvel 1','Captain Marvel',2019,128,80,59,9);




CREATE TABLE CHARACTERS 
    (CHARID              	NUMERIC(2)  IDENTITY(1, 1) NOT NULL,
     CHAR_NAME              VARCHAR(30),
     NICKNAME               VARCHAR(30),
	 ORIGIN					VARCHAR(30),
	 MOST_SPECIAL_SKILL		VARCHAR(30),
	 GOOD_GUY				BIT,
	 AFTER_IW_STATUS		BIT,
	 AFTER_EG_STATUS		BIT,
  CONSTRAINT CHARACTERS_PRIMARY_KEY PRIMARY KEY (CHARID));
  
INSERT INTO CHARACTERS VALUES ('Tony Stark','Iron Man','Human','armour, IQ',1,1,0);
INSERT INTO CHARACTERS VALUES ('Carol Danvers','Captain Marvel','Human/Kree','baddest MoFo in the Galaxy',1,1,0);
INSERT INTO CHARACTERS VALUES ('James Rhodes','War Machine','Human','armour',1,1,0);
INSERT INTO CHARACTERS VALUES ('Obidiah Stane','','Human','armour',0,null,null);
INSERT INTO CHARACTERS VALUES ('Ivan Vanko','','Human','armour',0,null,null);
INSERT INTO CHARACTERS VALUES ('Natalia Romanoff','Black Widow','Human','combat skills',1,1,0);
INSERT INTO CHARACTERS VALUES ('Nick Fury','','Human','SHIELD',1,0,0);
INSERT INTO CHARACTERS VALUES ('Aldrich Killian','','Human','IQ',0,null,null);
INSERT INTO CHARACTERS VALUES ('Bruce Banner','Hulk','Human','Hulk',1,1,0);
INSERT INTO CHARACTERS VALUES ('Thaddeus Ross','General Ross','Human','ARMY',0,null,null);
INSERT INTO CHARACTERS VALUES ('Thor Odinson','Thor','Asgard','God',1,1,0);
INSERT INTO CHARACTERS VALUES ('Odin Borson','Odin','Asgard','God',1,0,0);
INSERT INTO CHARACTERS VALUES ('Loki Odinson','Loki','Jötunn','God',1,0,0);
INSERT INTO CHARACTERS VALUES ('Malekith the Accursed','Malekith','Dark Elf','STONE',0,null,null);
INSERT INTO CHARACTERS VALUES ('Hela','','Asgard','God',0,0,0);
INSERT INTO CHARACTERS VALUES ('Steve Rodgers','Captain America','Human','Superhuman strength',1,1,0);
INSERT INTO CHARACTERS VALUES ('Johann Schmidt','Red Skull','Human','STONE',0,1,0);
INSERT INTO CHARACTERS VALUES ('Bucky Barnes','Winter Soldier','Human','metal arm',1,0,0);
INSERT INTO CHARACTERS VALUES ('Armin Zola','','Human','IQ',0,null,null);
INSERT INTO CHARACTERS VALUES ('Helmut Zemo','Baron Zemo','Human','army trained',0,null,null);
INSERT INTO CHARACTERS VALUES ('Ultron','','Tony Starks workshop','internet',0,0,0);
INSERT INTO CHARACTERS VALUES ('Thanos','The Mad Titan','Titan','STONES, strength',0,1,0);
INSERT INTO CHARACTERS VALUES ('Peter Quill','Star Lord','Human/Spartoi','combat skills',1,0,0);
INSERT INTO CHARACTERS VALUES ('Gamora','','	Zen-Whoberis','combat skills',1,0,0);
INSERT INTO CHARACTERS VALUES ('Arthur Douglas','Drax the Destroyer','Human','Superhuman strength',1,0,0);
INSERT INTO CHARACTERS VALUES ('Groot','','Flora Colossus','Superhuman strength',1,0,0);
INSERT INTO CHARACTERS VALUES ('Rocket Racoon','Rabit','Halfworld','IQ',1,1,0);
INSERT INTO CHARACTERS VALUES ('Ronan the Accuser','','Kree','Superhuman strength',0,0,0);
INSERT INTO CHARACTERS VALUES ('Ego','Ego the Living Planet','Sentient Planet','He is a planet...',0,0,0);
INSERT INTO CHARACTERS VALUES ('Scott Lang','Ant-Man','Human','suit',1,1,0);
INSERT INTO CHARACTERS VALUES ('Sam Wilson','Falcon','Human','wing-suit',1,0,0);
INSERT INTO CHARACTERS VALUES ('Darren Cross','Yellowjacket','Human','suit',0,0,0);
INSERT INTO CHARACTERS VALUES ('Hope Van Dyne','Wasp','Human','suit',1,0,0);
INSERT INTO CHARACTERS VALUES ('Ava','Ghost','Human','quantum something',0,0,0);
INSERT INTO CHARACTERS VALUES ('Stephen Strange','Doctor Strange','Human','magic',1,0,0);
INSERT INTO CHARACTERS VALUES ('Kaecilius','','Human','magic',0,0,0);
INSERT INTO CHARACTERS VALUES ('Peter Parker','Spider-Man','Human','spider stuffs',1,0,0);
INSERT INTO CHARACTERS VALUES ('Adrian Toomes','Vulture','Human','armour',0,null,null);
INSERT INTO CHARACTERS VALUES ('TChalla','Black Panther','Human','vibranium stuffs',1,0,0);
INSERT INTO CHARACTERS VALUES ('Erik Killmonger','Killmonger','Human','vibranium stuffs',0,0,0);
INSERT INTO CHARACTERS VALUES ('Clint Barton','Hawkeye','Human','marksman',1,1,0);




CREATE TABLE MOVIESCHARACTERSconnector
	(CONNID					NUMERIC(3)  IDENTITY(1, 1) NOT NULL,
	 CHARID					NUMERIC(2) NOT NULL,
	 MOVID					NUMERIC(2) NOT NULL,
  CONSTRAINT MOVIES_CONNECTOR_FOREIGN_KEY FOREIGN KEY (MOVID) REFERENCES MOVIES (MOVID),
  CONSTRAINT CHARACTERS_CONNECTOR_FOREIGN_KEY FOREIGN KEY (CHARID) REFERENCES CHARACTERS (CHARID),
  CONSTRAINT MOVIESCHARACTERS_PRIMARY_KEY PRIMARY KEY (CONNID));

INSERT INTO MOVIESCHARACTERSconnector VALUES (1,1);
INSERT INTO MOVIESCHARACTERSconnector VALUES (1,2);
INSERT INTO MOVIESCHARACTERSconnector VALUES (1,3);
INSERT INTO MOVIESCHARACTERSconnector VALUES (1,11);
INSERT INTO MOVIESCHARACTERSconnector VALUES (1,12);
INSERT INTO MOVIESCHARACTERSconnector VALUES (1,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (1,14);
INSERT INTO MOVIESCHARACTERSconnector VALUES (1,10);
INSERT INTO MOVIESCHARACTERSconnector VALUES (1,20);

INSERT INTO MOVIESCHARACTERSconnector VALUES (2,22);
INSERT INTO MOVIESCHARACTERSconnector VALUES (2,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (3,1);
INSERT INTO MOVIESCHARACTERSconnector VALUES (3,2);
INSERT INTO MOVIESCHARACTERSconnector VALUES (3,3);
INSERT INTO MOVIESCHARACTERSconnector VALUES (3,12);
INSERT INTO MOVIESCHARACTERSconnector VALUES (3,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (3,14);
INSERT INTO MOVIESCHARACTERSconnector VALUES (3,10);

INSERT INTO MOVIESCHARACTERSconnector VALUES (4,1);

INSERT INTO MOVIESCHARACTERSconnector VALUES (5,2);

INSERT INTO MOVIESCHARACTERSconnector VALUES (6,2);
INSERT INTO MOVIESCHARACTERSconnector VALUES (6,1);
INSERT INTO MOVIESCHARACTERSconnector VALUES (6,9);
INSERT INTO MOVIESCHARACTERSconnector VALUES (6,10);
INSERT INTO MOVIESCHARACTERSconnector VALUES (6,11);
INSERT INTO MOVIESCHARACTERSconnector VALUES (6,12);
INSERT INTO MOVIESCHARACTERSconnector VALUES (6,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (6,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (7,2);
INSERT INTO MOVIESCHARACTERSconnector VALUES (7,8);
INSERT INTO MOVIESCHARACTERSconnector VALUES (7,11);
INSERT INTO MOVIESCHARACTERSconnector VALUES (7,9);
INSERT INTO MOVIESCHARACTERSconnector VALUES (7,12);
INSERT INTO MOVIESCHARACTERSconnector VALUES (7,22);

INSERT INTO MOVIESCHARACTERSconnector VALUES (8,3);

INSERT INTO MOVIESCHARACTERSconnector VALUES (9,4);
INSERT INTO MOVIESCHARACTERSconnector VALUES (9,7);
INSERT INTO MOVIESCHARACTERSconnector VALUES (9,11);
INSERT INTO MOVIESCHARACTERSconnector VALUES (9,12);
INSERT INTO MOVIESCHARACTERSconnector VALUES (9,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (9,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (10,6);

INSERT INTO MOVIESCHARACTERSconnector VALUES (11,5);
INSERT INTO MOVIESCHARACTERSconnector VALUES (11,6);
INSERT INTO MOVIESCHARACTERSconnector VALUES (11,7);
INSERT INTO MOVIESCHARACTERSconnector VALUES (11,11);
INSERT INTO MOVIESCHARACTERSconnector VALUES (11,12);
INSERT INTO MOVIESCHARACTERSconnector VALUES (11,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (11,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (12,4);
INSERT INTO MOVIESCHARACTERSconnector VALUES (12,6);
INSERT INTO MOVIESCHARACTERSconnector VALUES (12,7);

INSERT INTO MOVIESCHARACTERSconnector VALUES (13,5);
INSERT INTO MOVIESCHARACTERSconnector VALUES (13,6);
INSERT INTO MOVIESCHARACTERSconnector VALUES (13,7);
INSERT INTO MOVIESCHARACTERSconnector VALUES (13,11);
INSERT INTO MOVIESCHARACTERSconnector VALUES (13,21);

INSERT INTO MOVIESCHARACTERSconnector VALUES (14,6);

INSERT INTO MOVIESCHARACTERSconnector VALUES (15,7);

INSERT INTO MOVIESCHARACTERSconnector VALUES (16,8);
INSERT INTO MOVIESCHARACTERSconnector VALUES (16,9);
INSERT INTO MOVIESCHARACTERSconnector VALUES (16,10);
INSERT INTO MOVIESCHARACTERSconnector VALUES (16,11);
INSERT INTO MOVIESCHARACTERSconnector VALUES (16,12);
INSERT INTO MOVIESCHARACTERSconnector VALUES (16,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (16,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (17,8);
INSERT INTO MOVIESCHARACTERSconnector VALUES (17,13);

INSERT INTO MOVIESCHARACTERSconnector VALUES (18,8);
INSERT INTO MOVIESCHARACTERSconnector VALUES (18,9);
INSERT INTO MOVIESCHARACTERSconnector VALUES (18,10);
INSERT INTO MOVIESCHARACTERSconnector VALUES (18,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (18,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (19,8);
INSERT INTO MOVIESCHARACTERSconnector VALUES (19,9);

INSERT INTO MOVIESCHARACTERSconnector VALUES (20,10);

INSERT INTO MOVIESCHARACTERSconnector VALUES (21,12);

INSERT INTO MOVIESCHARACTERSconnector VALUES (22,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (22,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (23,15);
INSERT INTO MOVIESCHARACTERSconnector VALUES (23,16);
INSERT INTO MOVIESCHARACTERSconnector VALUES (23,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (23,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (24,15);
INSERT INTO MOVIESCHARACTERSconnector VALUES (24,16);
INSERT INTO MOVIESCHARACTERSconnector VALUES (24,13);

INSERT INTO MOVIESCHARACTERSconnector VALUES (25,15);
INSERT INTO MOVIESCHARACTERSconnector VALUES (25,16);
INSERT INTO MOVIESCHARACTERSconnector VALUES (25,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (25,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (26,15);
INSERT INTO MOVIESCHARACTERSconnector VALUES (26,16);
INSERT INTO MOVIESCHARACTERSconnector VALUES (26,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (26,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (27,15);
INSERT INTO MOVIESCHARACTERSconnector VALUES (27,16);
INSERT INTO MOVIESCHARACTERSconnector VALUES (27,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (27,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (28,22);
INSERT INTO MOVIESCHARACTERSconnector VALUES (28,15);

INSERT INTO MOVIESCHARACTERSconnector VALUES (29,16);

INSERT INTO MOVIESCHARACTERSconnector VALUES (30,17);
INSERT INTO MOVIESCHARACTERSconnector VALUES (30,18);
INSERT INTO MOVIESCHARACTERSconnector VALUES (30,10);
INSERT INTO MOVIESCHARACTERSconnector VALUES (30,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (31,9);
INSERT INTO MOVIESCHARACTERSconnector VALUES (31,10);
INSERT INTO MOVIESCHARACTERSconnector VALUES (31,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (31,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (32,17);

INSERT INTO MOVIESCHARACTERSconnector VALUES (33,17);

INSERT INTO MOVIESCHARACTERSconnector VALUES (34,20);

INSERT INTO MOVIESCHARACTERSconnector VALUES (35,19);
INSERT INTO MOVIESCHARACTERSconnector VALUES (35,7);
INSERT INTO MOVIESCHARACTERSconnector VALUES (35,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (35,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (36,19);

INSERT INTO MOVIESCHARACTERSconnector VALUES (37,10);
INSERT INTO MOVIESCHARACTERSconnector VALUES (37,20);
INSERT INTO MOVIESCHARACTERSconnector VALUES (37,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (37,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (38,20);

INSERT INTO MOVIESCHARACTERSconnector VALUES (39,21);
INSERT INTO MOVIESCHARACTERSconnector VALUES (39,10);
INSERT INTO MOVIESCHARACTERSconnector VALUES (39,13);
INSERT INTO MOVIESCHARACTERSconnector VALUES (39,14);

INSERT INTO MOVIESCHARACTERSconnector VALUES (40,21);

INSERT INTO MOVIESCHARACTERSconnector VALUES (41,15);
INSERT INTO MOVIESCHARACTERSconnector VALUES (41,16);
INSERT INTO MOVIESCHARACTERSconnector VALUES (41,17);