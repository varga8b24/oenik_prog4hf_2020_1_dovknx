//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MarvelMovies.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class CHARACTER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CHARACTER()
        {
            this.MOVIESCHARACTERSconnectors = new HashSet<MOVIESCHARACTERSconnector>();
        }
    
        public decimal CHARID { get; set; }
        public string CHAR_NAME { get; set; }
        public string NICKNAME { get; set; }
        public string ORIGIN { get; set; }
        public string MOST_SPECIAL_SKILL { get; set; }
        public Nullable<bool> GOOD_GUY { get; set; }
        public Nullable<bool> AFTER_IW_STATUS { get; set; }
        public Nullable<bool> AFTER_EG_STATUS { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MOVIESCHARACTERSconnector> MOVIESCHARACTERSconnectors { get; set; }
    }
}
