﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarvelMovies.Data
{
    public static class Modell_Ext
    {
        /// <summary>
        /// Modell típusok listája
        /// </summary>
        public static List<Type> ModellTypes = new List<Type>()
        {
            typeof(COLLECTION),
            typeof(MOVy),
            typeof(MOVIESCHARACTERSconnector),
            typeof(CHARACTER)
        };
        /// <summary>
        /// ToString metódus felülírása
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>saját string reprezentáció</returns>
        public static string ModellToString(object obj)
        {
            Type selfType = obj.GetType();

            string data = selfType.Name.IndexOf('_') == -1
                ? $"{selfType.Name}:"
                : $"{selfType.Name.Remove(selfType.Name.IndexOf('_'))}:";

            foreach (var propertyInfo in selfType.GetProperties())
            {
                if ((propertyInfo.PropertyType.IsGenericType && !propertyInfo.PropertyType.IsNullableT()) || ModellTypes.Contains(propertyInfo.PropertyType))
                {
                    continue;
                }

                data += $" {propertyInfo.Name}: {propertyInfo.GetValue(obj)} -";
            }

            return data.Remove(data.Length - 2);
        }
    }

    /// <summary>
    /// CHARACTER ToString metódusának fwlülírása
    /// </summary>
    public partial class CHARACTER
    {
        public override string ToString()
        {
            return Modell_Ext.ModellToString(this);
        }
    }

    /// <summary>
    /// COLLECTION ToString metódusának fwlülírása
    /// </summary>
    public partial class COLLECTION
    {
        public override string ToString()
        {
            return Modell_Ext.ModellToString(this);
        }
    }

    /// <summary>
    /// MOVy ToString metódusának fwlülírása
    /// </summary>
    public partial class MOVy
    {
        public override string ToString()
        {
            return Modell_Ext.ModellToString(this);
        }
    }
}
