﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarvelMovies.Web.Models
{
    public class MoviesViewModel
    {
        public Movie EditedMovie { get; set; }
        public Movie RemovedMovie { get; set; }
        public List<Movie> ListOfMovies { get; set; }

    }
}