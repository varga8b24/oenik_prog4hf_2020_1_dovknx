﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarvelMovies.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MarvelMovies.Data.MOVy, MarvelMovies.Web.Models.Movie>().
                    ForMember(dest => dest.MOVID, map => map.MapFrom(src => src.MOVID)).
                    ForMember(dest => dest.CRONOLOGICAL_ORDER, map => map.MapFrom(src => src.CRONOLOGICAL_ORDER)).
                    ForMember(dest => dest.MOVIE_NAME, map => map.MapFrom(src => src.MOVIE_NAME)).
                    ForMember(dest => dest.TITLE, map => map.MapFrom(src => src.TITLE)).
                    ForMember(dest => dest.RELEASE_YEAR, map => map.MapFrom(src => src.RELEASE_YEAR)).
                    ForMember(dest => dest.RUNTIME, map => map.MapFrom(src => src.RUNTIME)).
                    ForMember(dest => dest.TOMATOMETER, map => map.MapFrom(src => src.TOMATOMETER)).
                    ForMember(dest => dest.AUDIENCE_SCORE, map => map.MapFrom(src => src.AUDIENCE_SCORE));

                cfg.CreateMap<MarvelMovies.Web.Models.Movie, MarvelMovies.Data.MOVy>().
                    ForMember(dest => dest.MOVID, map => map.MapFrom(src => src.MOVID)).
                    ForMember(dest => dest.CRONOLOGICAL_ORDER, map => map.MapFrom(src => src.CRONOLOGICAL_ORDER)).
                    ForMember(dest => dest.MOVIE_NAME, map => map.MapFrom(src => src.MOVIE_NAME)).
                    ForMember(dest => dest.TITLE, map => map.MapFrom(src => src.TITLE)).
                    ForMember(dest => dest.RELEASE_YEAR, map => map.MapFrom(src => src.RELEASE_YEAR)).
                    ForMember(dest => dest.RUNTIME, map => map.MapFrom(src => src.RUNTIME)).
                    ForMember(dest => dest.TOMATOMETER, map => map.MapFrom(src => src.TOMATOMETER)).
                    ForMember(dest => dest.AUDIENCE_SCORE, map => map.MapFrom(src => src.AUDIENCE_SCORE));
                    //ForMember(dest => dest.MOVIESCHARACTERSconnectors, map => map.MapFrom(src => src.;
            });

            return config.CreateMapper();
        }
    }
}