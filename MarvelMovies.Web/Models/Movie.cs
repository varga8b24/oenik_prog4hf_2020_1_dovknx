﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MarvelMovies.Web.Models
{
    public class Movie
    {
        [Display(Name = "Movie Id")]
        [Required]
        public int MOVID { get; set; }

        [Display(Name = "Cronological Order")]
        [Required]
        public int CRONOLOGICAL_ORDER { get; set; }

        [Display(Name = "Movie Name")]
        [Required]
        public string MOVIE_NAME { get; set; }

        [Display(Name = "Movie Title")]
        [Required]
        public string TITLE { get; set; }

        [Display(Name = "Movie Release Year")]
        [Required]
        public int RELEASE_YEAR { get; set; }

        [Display(Name = "Movie Runtime (in minutes)")]
        [Required]
        public int RUNTIME { get; set; }

        [Display(Name = "Movie Tomatometer (1-100 scale)")]
        [Required]
        public int TOMATOMETER { get; set; }

        [Display(Name = "Movie Audience Score (1-100 scale)")]
        [Required]
        public int AUDIENCE_SCORE { get; set; }

        [Display(Name = "Collection Name")]
        [Required]
        public string CollectionName { get; set; }
    }
}