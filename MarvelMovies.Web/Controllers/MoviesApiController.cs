﻿using AutoMapper;
using MarvelMovies.Data;
using MarvelMovies.Logic;
using MarvelMovies.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MarvelMovies.Web.Controllers
{
    public class MoviesApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        ILogic logic;
        IMapper mapper;

        public MoviesApiController()
        {
            logic = new MarvelMoviesLogicTool();
            mapper = MapperFactory.CreateMapper();
        }

        // GET /api/MoviesApi/all
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Movie> GetAll()
        {
            var movies = logic.ListMovies();
            return mapper.Map<IEnumerable<Data.MOVy>, List<Models.Movie>>(movies);
        }

        // GET /api/MoviesApi/del/1
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneMovie(int id)
        {
            bool success = logic.DeleteMovie(id);

            return new ApiResult() { OperationResult = success };
        }

        // POST /api/MoviesApi/add + movie
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneMovie(Movie movie)
        {
            var movieToAdd = mapper.Map<Movie, MOVy>(movie);

            ((MarvelMoviesLogicTool)logic).CreateMovie(movieToAdd, movie.CollectionName);

            return new ApiResult() { OperationResult = true };
        }

        // GET /api/MoviesApi/mod + movie
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneMovie(Movie movie)
        {
            var movieToMod = mapper.Map<Movie, MOVy>(movie);

            bool success = logic.UpdateMovie(movieToMod);

            return new ApiResult() { OperationResult = success };
        }
    }
}
