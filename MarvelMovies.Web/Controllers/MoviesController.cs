﻿using AutoMapper;
using MarvelMovies.Data;
using MarvelMovies.Logic;
using MarvelMovies.Repository;
using MarvelMovies.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarvelMovies.Web.Controllers
{
    public class MoviesController : Controller
    {
        ILogic logic;
        IMapper mapper;
        MoviesViewModel vm;

        public MoviesController()
        {
            vm = new MoviesViewModel();

            logic = new MarvelMoviesLogicTool();
            mapper = MapperFactory.CreateMapper();

            vm.EditedMovie = new Movie();
            var moviesVar = logic.ListMovies();

            vm.ListOfMovies = mapper.Map<IEnumerable<Data.MOVy>, List<Models.Movie>>(moviesVar);
        }


        private Movie GetMovie(int id)
        {
            MOVy movie = logic.ListMovies().FirstOrDefault(x => x.MOVID == id);

            return mapper.Map<MOVy, Movie>(movie, new Movie());
        }



        // GET: Movies
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("MoviesIndex", vm);
        }

        // GET: Movies/Details/5
        public ActionResult Details(int id)
        {
            return View("MovieDetails", GetMovie(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";

            if (logic.DeleteMovie(id)) TempData["editResult"] = "Delete OK";

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedMovie = GetMovie(id);
            return View("MoviesIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Movie movie, string editAction)
        {
            if (ModelState.IsValid && movie != null)
            {
                var editOrAddEntity = mapper.Map<Movie, MOVy>(movie);

                if (editAction == "Edit")
                {
                    TempData["editResult"] = "Edit OK";

                    if (!logic.UpdateMovie(editOrAddEntity))
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }
                else
                {
                    TempData["editResult"] = "Add new OK";

                    ((MarvelMoviesLogicTool)logic).CreateMovie(editOrAddEntity, movie.CollectionName);
                }

                return RedirectToAction("Index");
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedMovie = movie;

                return View("MoviesIndex", vm);
            }
        }
    }
}
