﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace MarvelMovies.Wpf
{
    class MovieVM : ObservableObject
    {
        private int id;
        private string name;
        private string title;
        private int releaseYear;
        private int runtime;
        private int tomatometer;
        private int audienceScore;
        private string collectionName;

        public int MOVID
        {
            get { return id; }
            set { Set(ref id, value); }
        }

        public string MOVIE_NAME
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        public string TITLE
        {
            get { return title; }
            set { Set(ref title, value); }
        }

        public int RELEASE_YEAR
        {
            get { return releaseYear; }
            set { Set(ref releaseYear, value); }
        }

        public int RUNTIME
        {
            get { return runtime; }
            set { Set(ref runtime, value); }
        }

        public int TOMATOMETER
        {
            get { return tomatometer; }
            set { Set(ref tomatometer, value); }
        }

        public int AUDIENCE_SCORE
        {
            get { return audienceScore; }
            set { Set(ref audienceScore, value); }
        }

        public string CollectionName
        {
            get { return collectionName; }
            set { Set(ref collectionName, value); }
        }

        public void CopyFrom(MovieVM other)
        {
            if (other == null) return;
            this.MOVID = other.id;
            this.MOVIE_NAME = other.name;
            this.TITLE = other.title;
            this.RELEASE_YEAR = other.releaseYear;
            this.RUNTIME = other.runtime;
            this.TOMATOMETER = other.tomatometer;
            this.AUDIENCE_SCORE = other.audienceScore;
            this.CollectionName = other.collectionName;
        }


    }
}
