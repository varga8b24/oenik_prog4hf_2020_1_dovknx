﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MarvelMovies.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:53815/api/MoviesApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed succesfully" : "Operation failed";
            Messenger.Default.Send(msg, "MovieResult");
        }

        public List<MovieVM> ApiGetMovies()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<MovieVM>>(json);
            return list;
        }

        public void ApiDelMovie(MovieVM movie)
        {
            bool success = false;
            if (movie != null)
            {
                string json = client.GetStringAsync(url + "del/" + movie.MOVID).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditMovie(MovieVM movie, bool isEditing)
        {
            if (movie == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(MovieVM.MOVID), movie.MOVID.ToString());
            postData.Add(nameof(MovieVM.MOVIE_NAME), movie.MOVIE_NAME);
            postData.Add(nameof(MovieVM.TITLE), movie.TITLE);
            postData.Add(nameof(MovieVM.RELEASE_YEAR), movie.RELEASE_YEAR.ToString());
            postData.Add(nameof(MovieVM.RUNTIME), movie.RUNTIME.ToString());
            postData.Add(nameof(MovieVM.TOMATOMETER), movie.TOMATOMETER.ToString());
            postData.Add(nameof(MovieVM.AUDIENCE_SCORE), movie.AUDIENCE_SCORE.ToString());
            postData.Add(nameof(MovieVM.CollectionName), movie.CollectionName);  //ADD-nál létező Collection Name-et kell megadni, hogy sikeres legyen, mint pl: SPIDER-MAN, IRON MAN, etc.

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditMovie(MovieVM movie, Func<MovieVM, bool> editor) //ADD-nál létező Collection Name-et kell megadni, hogy sikeres legyen, mint pl: SPIDER-MAN, IRON MAN, etc.
        {
            MovieVM clone = new MovieVM();
            if (movie != null) clone.CopyFrom(movie);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (movie != null) success = ApiEditMovie(clone, true);
                else success = ApiEditMovie(clone, false);
            }
            SendMessage(success == true);
        }

    }
}
