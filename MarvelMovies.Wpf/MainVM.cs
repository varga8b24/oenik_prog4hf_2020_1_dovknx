﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MarvelMovies.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;

        private MovieVM selectedMovie;
        private ObservableCollection<MovieVM> allMovies;

        public MovieVM SelectedMovie
        {
            get { return selectedMovie; }
            set { Set(ref selectedMovie, value); }
        }

        public ObservableCollection<MovieVM> AllMovies
        {
            get { return allMovies; }
            set { Set(ref allMovies, value); }
        }
        
        public ICommand AddCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand LoadCmd { get; private set; }

        public Func<MovieVM, bool> EditorFunc { get; set; }


        public MainVM()
        {
            logic = new MainLogic();

            DelCmd = new RelayCommand(()=>logic.ApiDelMovie(selectedMovie));
            AddCmd = new RelayCommand(() => logic.EditMovie(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditMovie(selectedMovie, EditorFunc));
            LoadCmd = new RelayCommand(() => AllMovies = new ObservableCollection<MovieVM>(logic.ApiGetMovies()));
        }

    }
}
