﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using MarvelMovies.Data;
using MarvelMovies.Repository;

namespace MarvelMovies.Logic
{
    /// <summary>
    /// Repository metódusakat hívó Business logic layer osztály
    /// </summary>
    public class MarvelMoviesLogicTool : ILogic
    {
        IRepository<CHARACTER> characterRepository;
        IRepository<COLLECTION> collectionRepository;
        IRepository<MOVy> movieRepository;
        IRepository<MOVIESCHARACTERSconnector> connectorRepository;

        /// <summary>
        /// CHARACTER Logic.Test kiegészítő konstruktor
        /// </summary>
        /// <param name="characterRepository"></param>
        public MarvelMoviesLogicTool(IRepository<CHARACTER> characterRepository)
        {
            this.characterRepository = characterRepository;
        }

        /// <summary>
        /// COLLECTION Logic.Test kiegészítő konstruktor
        /// </summary>
        /// <param name="collectionRepository"></param>
        public MarvelMoviesLogicTool(IRepository<COLLECTION> collectionRepository)
        {
            this.collectionRepository = collectionRepository;
        }

        /// <summary>
        /// MOVy Logic.Test kiegészítő konstruktor
        /// </summary>
        /// <param name="movieRepository"></param>
        public MarvelMoviesLogicTool(IRepository<MOVy> movieRepository)
        {
            this.movieRepository = movieRepository;
        }

        /// <summary>
        /// MOVIESCHARACTERSconnector Logic.Test kiegészítő konstruktor
        /// </summary>
        /// <param name="connectorRepository"></param>
        public MarvelMoviesLogicTool(IRepository<MOVIESCHARACTERSconnector> connectorRepository)
        {
            this.connectorRepository = connectorRepository;
        }


        public MarvelMoviesLogicTool(IRepository<CHARACTER> characterRepository, IRepository<COLLECTION> collectionRepository, IRepository<MOVy> movieRepository, IRepository<MOVIESCHARACTERSconnector> connectorRepository)
        {
            this.characterRepository = characterRepository;
            this.collectionRepository = collectionRepository;
            this.movieRepository = movieRepository;
            this.connectorRepository = connectorRepository;
        }

        public MarvelMoviesLogicTool()
        {
            characterRepository = new CharacterRepository();
            collectionRepository = new CollectionRepository();
            movieRepository = new MovieRepository();
            connectorRepository = new ConnectorRepository();
        }



        #region character

        /// <summary>
        /// CHARACTER műveletek
        /// </summary>
        public decimal AddCharacter(CHARACTER obj)
        {
            return characterRepository.Create(obj);
        }

        public bool DeleteCharacter(decimal id)
        {
            return characterRepository.Delete(id);
        }

        public IEnumerable<CHARACTER> ListCharacters()
        {
            return characterRepository.Read();
        }

        public bool UpdateCharacter(CHARACTER obj)
        {
            return characterRepository.Update(obj);
        }
        #endregion

        #region collection
        /// <summary>
        /// CHARACTER műveletek
        /// </summary>
        public IEnumerable<COLLECTION> ListCollections()
        {
            return collectionRepository.Read();
        }

        public decimal AddCollection(COLLECTION obj)
        {
            return collectionRepository.Create(obj);
        }

        public bool UpdateCollection(COLLECTION obj)
        {
            return collectionRepository.Update(obj);
        }

        public bool DeleteCollection(decimal id)
        {
            return collectionRepository.Delete(id);
        }
        #endregion

        #region movies

        /// <summary>
        /// MOVy műveletek
        /// </summary>
        /// 

        

        public IEnumerable<MOVy> ListMovies()
        {
            return movieRepository.Read();
        }

        public decimal CreateMovie(MOVy obj, string collectionName)
        {
            decimal collectionId = collectionRepository.Read().FirstOrDefault(x => x.TITLE == collectionName)?.COLLID ?? 0;

            if (collectionId == 0)
            {
                return 0;
            }

            obj.COLLID = collectionId;

            return AddMovie(obj);
        }

        public decimal AddMovie(MOVy obj)
        {
            return movieRepository.Create(obj);
        }

        public bool UpdateMovie(MOVy obj)
        {
            return movieRepository.Update(obj);
        }

        public bool DeleteMovie(decimal id)
        {
            return movieRepository.Delete(id);
        }
        #endregion

        #region Not CRUD methods
        /// <summary>
        /// Nem CRUD műveletek
        /// </summary>
        public IEnumerable<MOVy> GetCharactersMoviesandInfoPage(string characterName)
        {

            var characterNamesCharID = (from character in characterRepository.Read()
                                        where character.NICKNAME.ToLower().Contains(characterName) || character.CHAR_NAME.ToLower().Contains(characterName)
                                        select character.CHARID).FirstOrDefault();

            var characterNamesMovies = from movies in connectorRepository.Read()
                                       join movIDsFROMconnector in movieRepository.Read()
                                       on movies.MOVID equals movIDsFROMconnector.MOVID
                                       join charIDsFROMconnector in characterRepository.Read()
                                       on movies.CHARID equals charIDsFROMconnector.CHARID
                                       where movies.CHARID == characterNamesCharID
                                       select movIDsFROMconnector;

            return characterNamesMovies;
        }

        public MOVy GetMovieWithTheMostCharactersInIt()
        {
            var connectorGroupByMoviesCount = from connector in connectorRepository.Read().ToList()
                                              group connector by connector.MOVID into g
                                              select new
                                              {
                                                  MOVID = g.Key,
                                                  CHAR_COUNT = g.Count()
                                              };

            var movieWithTheMostCharacterInIt = (from movie in movieRepository.Read().ToList()
                                                 where movie.MOVID == connectorGroupByMoviesCount.OrderByDescending(x => x.CHAR_COUNT).First().MOVID
                                                 select movie).FirstOrDefault();

            return movieWithTheMostCharacterInIt;

        }

        public IEnumerable<CHARACTER> GetTop3CharacterWithMostAppearance()
        {
            var connectorGroupByCharacterCount = from connector in connectorRepository.Read()
                                                 group connector by connector.CHARID into g
                                                 select new
                                                 {
                                                     CHARID = g.Key,
                                                     MOV_COUNT = g.Count()
                                                 };

            var top3charAppearance = from characters in characterRepository.Read()
                                     where connectorGroupByCharacterCount.OrderByDescending(x => x.MOV_COUNT).Take(3).Select(x => x.CHARID).Contains(characters.CHARID)
                                     select characters;

            return top3charAppearance;
        }

        public IEnumerable<MOVy> GetTop3MoviesByAveragedRating()
        {
            var moviesAverageRating = from movies in movieRepository.Read()
                                      orderby ((movies.AUDIENCE_SCORE + movies.TOMATOMETER) / 2) descending

                                      select new
                                      {
                                          TITLE = movies.TITLE,
                                          AVERAGE = ((double)(Math.Round((decimal)((movies.AUDIENCE_SCORE + movies.TOMATOMETER) / 2), 1)))
                                      };

            var tomatomaterRatings = (from movies in movieRepository.Read()
                                      orderby movies.TOMATOMETER descending
                                      select movies)
                                       .Take(3);

            return tomatomaterRatings;
        }
        #endregion

        #region JavaEndpoint
        /// <summary>
        /// Java End Point művelet
        /// </summary>
        /// <param name="url"></param>
        public void InsertJavaEndPointOutputToDb(string url)
        {
            XDocument doc = null;

            using (var client = new WebClient())
            {
                string xml_text = client.DownloadString(url);
                doc = XDocument.Parse(xml_text);
                Console.WriteLine(doc);
            }

            Console.WriteLine();
            Console.WriteLine("Do you want to add these to the database? (type yes or no)");

            string javaEnpointAnswer = Console.ReadLine();
            if (javaEnpointAnswer.ToLower() == "yes")
            {
                var javaEndpointCollectionsOutput = (from collection in doc.Descendants("collections")
                                                     select new COLLECTION
                                                     {
                                                         TITLE = collection.Element("title").Value,
                                                         CREATOR = collection.Element("creator").Value,
                                                         FIRST_APPEARANCE_YEAR = decimal.Parse(collection.Element("firstApperanceYear").Value),
                                                         MOVIE_RELEASE_YEAR = decimal.Parse(collection.Element("movieReleaseYear").Value),
                                                         MOVIES_COUNT = decimal.Parse(collection.Element("moviesCount").Value)
                                                     }).ToList();

                var javaEndpointMoviesOutput = (from movie in doc.Descendants("movies")
                                                select new MOVy
                                                {
                                                    MOVID = decimal.Parse(movie.Element("movid").Value),
                                                    CRONOLOGICAL_ORDER = decimal.Parse(movie.Element("cronological_order").Value),
                                                    MOVIE_NAME = movie.Element("movie_name").Value,
                                                    TITLE = movie.Element("title").Value,
                                                    RELEASE_YEAR = decimal.Parse(movie.Element("release_year").Value),
                                                    RUNTIME = decimal.Parse(movie.Element("runtime").Value),
                                                    TOMATOMETER = decimal.Parse(movie.Element("tomatometer").Value),
                                                    AUDIENCE_SCORE = decimal.Parse(movie.Element("audience_score").Value),
                                                }).ToList();

                javaEndpointCollectionsOutput.First().MOVIES = javaEndpointMoviesOutput;

                JAVAEndpointWork(javaEndpointCollectionsOutput);
            }
        }

        private void JAVAEndpointWork(IEnumerable<COLLECTION> output)
        {

            foreach (var item in output)
            {
                collectionRepository.Create(item);
            }
        }


        #endregion
    }
}
