﻿using MarvelMovies.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarvelMovies.Logic
{
    /// <summary>
    /// Repository műveleteket hívó BLL réteg általános interface-e
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// COLLECTION tábla műveletei
        /// </summary>
        /// <returns></returns>
        IEnumerable<COLLECTION> ListCollections();
        decimal AddCollection(COLLECTION obj);
        bool UpdateCollection(COLLECTION obj);
        bool DeleteCollection(decimal id);

        /// <summary>
        /// MOVy tábla műveletei
        /// </summary>
        /// <returns></returns>
        IEnumerable<MOVy> ListMovies();
        decimal AddMovie(MOVy obj);
        bool UpdateMovie(MOVy obj);
        bool DeleteMovie(decimal id);

        /// <summary>
        /// CHARACTER tábla műveletei
        /// </summary>
        /// <returns></returns>
        IEnumerable<CHARACTER> ListCharacters();
        decimal AddCharacter(CHARACTER obj);
        bool UpdateCharacter(CHARACTER obj);
        bool DeleteCharacter(decimal id);

        /// <summary>
        /// Nem CRUD műveletek
        /// </summary>
        /// <returns></returns>
        MOVy GetMovieWithTheMostCharactersInIt();
        IEnumerable<MOVy> GetTop3MoviesByAveragedRating();
        IEnumerable<CHARACTER> GetTop3CharacterWithMostAppearance();
        IEnumerable<MOVy> GetCharactersMoviesandInfoPage(string characterName);
    }
}
